import axios from 'axios';
import * as $cookie from 'vue-cookie';

const API_URL = process.env.API_URL || 'http://localhost:3000/api/v1/';

export default axios.create({
  baseURL: API_URL,
  headers: {
    Authorization: `Bearer ${$cookie.get('token')}`,
  },
});
